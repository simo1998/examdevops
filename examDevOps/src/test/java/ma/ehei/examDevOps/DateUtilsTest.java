package ma.ehei.examDevOps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DateUtilsTest {

	  private DateUtils DateUtils;


	@Before
	    public void before() throws Exception {
	        DateUtils = new DateUtils();
	    }
	@Test
	public void testAjoutJours() {
		DateUtils dateUtils = new DateUtils();
		String dateString = "10/04/2023";
		int nbrJour = 1;
		Date expectedDate = DateTime.parse("11/04/2023", DateTimeFormat.forPattern("dd/MM/yyyy")).toDate();
		assertEquals(expectedDate, dateUtils.ajoutJours(dateString, nbrJour));
	}
	 @Test(expected = IllegalArgumentException.class)
	public void testAjoutJoursNegative() {
		DateUtils dateUtils = new DateUtils();
		String dateString = "10/04/2023";
		int nbrJour = -1;
		 dateUtils.ajoutJours(dateString, nbrJour);
	}


	@Test
	public void testAjoutJoursZero() {
		DateUtils dateUtils = new DateUtils();
		String dateString = "10/04/2023";
		int nbrJour = 0;
		Date expectedDate = DateTime.parse("10/04/2023", DateTimeFormat.forPattern("dd/MM/yyyy")).toDate();
		assertEquals(expectedDate, dateUtils.ajoutJours(dateString, nbrJour));
	}
	   @After
	    public void after() throws Exception {
		   
	    }
	   

}
